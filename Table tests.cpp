/*
 * tablieTesty.cpp
 *
 *  Created on: 22.04.2017
 *      Author: Magda
 */

#include "gtest/gtest.h"
#include "Table.h"


class ArrayOperationTestFixture : public testing::Test { //klasa pomocnicza
protected:
	Table myTestTab;
	void SetUp() { //konstruktor klasy pomocniczej
		myTestTab.createNewTable(12);
//		myTestTab.addElementToTheEnd(40);
	}
	virtual void TearDown() { //destruktor klasy pomocniczej
	}
};

TEST_F(ArrayOperationTestFixture, tabSetCorrectly) {
	ASSERT_EQ(-1,myTestTab.getTableElement(-12));
	ASSERT_EQ(0,myTestTab.getTableElement(0));
	ASSERT_EQ(10,myTestTab.getTableElement(10));
	ASSERT_EQ(-1,myTestTab.getTableElement(13));

}

TEST_F(ArrayOperationTestFixture, returnFirstElementPointerCorrectly) {
	ASSERT_EQ(1,myTestTab.getFirstElementPointer());
}


TEST_F(ArrayOperationTestFixture, tabSizeCorrectly) {
	ASSERT_EQ(12,myTestTab.printTableSize());
}

TEST_F(ArrayOperationTestFixture, printMinElementCorrectly){
	ASSERT_EQ(0,myTestTab.printMinValue());
}

TEST_F(ArrayOperationTestFixture, printMaxElementCorrectly){
	ASSERT_EQ(11,myTestTab.printMaxValue());
}

TEST_F(ArrayOperationTestFixture, printElementFromIndexCorrectly){
	ASSERT_EQ(0,myTestTab.printElementFromIndex(0));
	ASSERT_EQ(8,myTestTab.printElementFromIndex(8));
	ASSERT_EQ(-1,myTestTab.printElementFromIndex(-3));

}

TEST_F(ArrayOperationTestFixture, addingElementToTheEndCorrectly){
	ASSERT_TRUE(myTestTab.addElementToTheEnd(2));
}

TEST_F(ArrayOperationTestFixture, addingElementToTheIndexCorrectly){
	ASSERT_TRUE(myTestTab.addElementToIndex(2,4));
	ASSERT_FALSE(myTestTab.addElementToIndex(14,4));

}

TEST_F(ArrayOperationTestFixture, addingElementIncreaseArraySize){
	ASSERT_EQ(9,myTestTab.addElementToIndexIncreaseArraySize(3,9));
}

TEST_F(ArrayOperationTestFixture, sumElementsCorrectly){
	ASSERT_EQ(66,myTestTab.sumTableElements());
}

TEST_F(ArrayOperationTestFixture, removeLastElementCorrectly){
	ASSERT_TRUE(myTestTab.removeLastElement());
}

TEST_F(ArrayOperationTestFixture, removeElementReduceSizeCorrectly){
	ASSERT_EQ(11,myTestTab.removeElementFromIndexReduceArraySize(4));
}

TEST_F(ArrayOperationTestFixture, raisingToPowerPositive){
	ASSERT_EQ(0,myTestTab.raisingElementToPower(0,2));
	ASSERT_EQ(9,myTestTab.raisingElementToPower(3,2));
	ASSERT_EQ(0,myTestTab.raisingElementToPower(12,2));
}




/*
 * table.h
 *
 *  Created on: 22.04.2017
 *      Author: Magda
 */

#ifndef TABLE_H_
#define TABLE_H_

#include <iostream>
using namespace std;


const int maxArraySize=1000;

class Table {
public:

//constructors
	Table(int size);
	Table();

//destructors
	virtual ~Table();

//methods
	void createNewTable(int size);
	double getTableElement(int index);
	double *getFirstElementPointer();
	int printTableSize();
	void showTableElements();
	double printElementFromIndex(int index);
	double printMinValue();
	double printMaxValue();
	bool addElementToTheEnd (double value);
	bool removeLastElement();
	int removeElementFromIndexReduceArraySize(int index);
	bool addElementToIndex(int index,double value);
	int addElementToIndexIncreaseArraySize(int index,double value);
	double sumTableElements();
	int raisingElementToPower(int index, int power);

//arguments
private:
	int size; //aktualny rozmiar
	double tab[maxArraySize];
	int index;
	double minValue;
	double maxValue;
	double value;
	int power;
	double average;


};

#endif /* TABLE_H_ */

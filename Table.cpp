/*
 * table.cpp
 *
 *  Created on: 22.04.2017
 *      Author: Magda
 */

#include "Table.h"
#include <cmath>

//constructors
Table::Table() {
	this->size=0;
	this->minValue=minValue;
	this->maxValue=maxValue;
	this->average=average;
	this->index=index;
	this->value=value;
}
//tablica dyanmiczna:
//Table::Table(int size) {
//	this->size=size;
//	tab=new double[size];
//	for (int i=0; i<size; i++) {
//		tab[i]=i;
//	}
//}

//destructors
Table::~Table() {
}

//methods
void Table::createNewTable(int size){
	for (int i=0;i<size;i++){
		tab[i]=i;
		this->size++;
	}
}


double Table::getTableElement(int index){
	if (index>size || index<0){
		return -1;
	}
	else
		return this->tab[index];
}

double* Table::getFirstElementPointer(){
	return tab;
}


int Table::printTableSize(){
	return size;
}

void Table::showTableElements() {
	for(int i=0; i<size; i++){
		cout << tab[i] <<endl;
	}
}

double Table::printElementFromIndex(int index){
	if (index>size || index<0){
		return -1;
	}
	return tab[index];
}

double Table::printMinValue(){
	minValue=*tab;
	for (int i=1; i<size; i++){
		if (minValue>tab[i]){
			minValue=tab[i];
		}
	}
	return minValue;
}

double Table::printMaxValue(){
	maxValue=*tab;
	for (int i=0;i<size;i++){
		if(maxValue<tab[i]){
			maxValue=tab[i];
		}
	}
	return maxValue;
}

bool Table::addElementToTheEnd (double value){
	size++;
	tab[size]=value;
	if(tab[size]==value){
		return true;
	}
	else
		return false;
}

bool Table::removeLastElement(){
	size--;
	if(this->printTableSize()!=size){
		return false;
	}
	else
		return true;
}

int Table::removeElementFromIndexReduceArraySize(int index){
	for (int i=index;i<size-1;i++){
		tab[i]=tab[i+1];
	}
	size=size-1;
	return size;
}


bool Table::addElementToIndex(int index, double value){
	tab[index]=value;
	if (this->getTableElement(index)==value){
		return true;
	}
	return false;
}

int Table::addElementToIndexIncreaseArraySize(int index, double value){
	size=size+1;
	for (int i=size-1;i>index;i++){
		tab[size]=tab[size-1];
	}
	tab[index]=value;
	return tab[index];
}

double Table::sumTableElements(){
	double sum=0;
	for(int i=0;i<size;i++){
		sum=sum+tab[i];
	}
	return sum;
}

int Table::raisingElementToPower(int index,int power){
	return pow(tab[index],power);
}




